FROM postgres:13.1-alpine
LABEL maintainer "Geek University <contato@geekuniversity.com.br>"
ENV POSTGRES_USER=filmes_user
ENV POSTGRES_PASSWORD=filmes_pass
ENV POSTGRES_DB=filmes_app
EXPOSE 5432
