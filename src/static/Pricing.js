import * as React from 'react';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import StarIcon from '@mui/icons-material/StarBorder';
import Typography from '@mui/material/Typography';
import GlobalStyles from '@mui/material/GlobalStyles';
import Container from '@mui/material/Container';
import CardMedia from '@mui/material/CardMedia';

function Copyright(props) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
      {'Copyright © '}
      
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const tiers = [
  {
    title: 'Amazon Prime',    
    image: <CardMedia
                component="img"
                sx={{
                  // 16:9
                  pt: '40%',
                }}
                image="https://uploads.metropoles.com/wp-content/uploads/2021/12/31121817/Lancamentos-de-Maio-no-Amazon-Prime-Video-1-600x400.jpg"
                alt="random"
             />,
    buttonText: <Button variant="contained" href='https://www.primevideo.com/?_encoding=UTF8&language=pt_br'>Assistir</Button>,
    
  },
  {
    title: 'Disney+',    
    image: <CardMedia
                component="img"
                sx={{
                  // 16:9
                  pt: '50%',
                }}
                image="https://t.ctcdn.com.br/WTMfUrus_QiUgecHUPjChmN4I5o=/512x288/smart/filters:format(webp)/i515941.png"
                alt="random"
             />,
    buttonText: <Button variant="contained" href='https://www.disneyplus.com/pt-br/login'>Assistir</Button>,
    
  },
  {
    title: 'Netflix',    
    image: <CardMedia
                component="img"
                sx={{
                  // 16:9
                  pt: '50%',
                }}
                image="https://www.canalsp.com.br/wp-content/uploads/2021/02/05cItXL96l4LE9n02WfDR0h-5..1582751026.png"
                alt="random"
            />,
    buttonText: <Button variant="contained" href='https://www.netflix.com/br/login'>Assistir</Button>,
    
  },
  {
    title: 'HBO MAX',    
    image: <CardMedia
                component="img"
                sx={{
                  // 16:9
                  pt: '40%',
                }}
                image="https://f.i.uol.com.br/fotografia/2022/08/04/165962505562ebde5fb0758_1659625055_3x2_md.jpg"
                alt="random"
            />,
    buttonText: <Button variant="contained" href='https://www.hbomax.com/br/pt'>Assitir</Button>,
    
  },
  {
    title: 'Star Plus',    
    image: <CardMedia
                component="img"
                sx={{
                  // 16:9
                  pt: '50%',
                }}
                image="https://assets.goal.com/v3/assets/bltcc7a7ffd2fbf71f5/blte0417493ff7d67bf/60e0dfe28aa56107a956a25d/edcc3dfe790c1296d2f3c802b8ddbb40cb215190.png"
                alt="random"
            />,
    buttonText: <Button variant="contained" href='https://www.starplus.com/pt-br/login'>Assitir</Button>,
    
  },
];

function PricingContent() {
  return (
    <React.Fragment>
      <GlobalStyles styles={{ ul: { margin: 0, padding: 0, listStyle: 'none' } }} />
      <CssBaseline />
      
      {/* Hero unit */}
      <Container disableGutters maxWidth="sm" component="main" sx={{ pt: 8, pb: 6 }}>
        <Typography
          component="h1"
          variant="h2"
          align="center"
          color="#ff9800"
          gutterBottom
        >
          Minha Lista
        </Typography>
        <Typography variant="h5" align="center" color="#ff9800" component="p">
          Assista suas principais series e Filmes, clicando no botão assistir.
        </Typography>
      </Container>
      {/* End hero unit */}
      <Container maxWidth="md" component="main">
        <Grid container spacing={5} alignItems="flex-end">
          {tiers.map((tier) => (
            // Enterprise card is full width at sm breakpoint
            <Grid
              item
              key={tier.title}
              xs={12}
              sm={tier.title === 'Enterprise' ? 12 : 6}
              md={4}
            >
              <Card>
                <CardHeader
                  title={tier.title}
                  
                  titleTypographyProps={{ align: 'center' }}
                  action={tier.title === 'Pro' ? <StarIcon /> : null}
                  subheaderTypographyProps={{
                    align: 'center',
                  }}
                  sx={{
                    backgroundColor: (theme) =>
                      theme.palette.mode === 'light'
                        ? theme.palette.grey[200]
                        : theme.palette.grey[700],
                  }}
                />
                <CardContent>                  
                  <image>
                    {tier.image}
                  </image>
                </CardContent>
                <CardActions>
                  <Button fullWidth>
                    {tier.buttonText}
                  </Button>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>
      {/* Footer */}
      
      {/* End footer */}
    </React.Fragment>
  );
}

export default function Pricing() {
  return <PricingContent />;
}