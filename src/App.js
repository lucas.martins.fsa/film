import React from "react";
import Pricing from "./static/Pricing";
import Typography from "@mui/material/Typography";

function App() {
  return (
    <div className="App" style={{ backgroundImage: 'url("https://portaldomedico.blob.core.windows.net/noticias/o63dz51jwwo6.jpg")'}}>
      <Typography>
        <Pricing/>
      </Typography>
    </div>
  );
}

export default App;
